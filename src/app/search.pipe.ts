import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(value: any, args: any, args1: any): any {
    let filter = args.toLocaleLowerCase();
    if(args1 === 'customer_username'){
    return filter ? value.filter(task => task.customer_username.toLocaleLowerCase().indexOf(filter)!=-1):value;
    }
    if(args1 === 'user_name'){
    return filter ? value.filter(task => task.user_name.toLocaleLowerCase().indexOf(filter)!=-1):value;
    }
  }

}
