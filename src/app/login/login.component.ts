import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,Validators} from '@angular/forms';
import { Router  } from '@angular/router';
import { ApiService } from '../services/api.service';
import { CustomValidators } from 'ng2-validation';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import {CookiesService} from '../services/cookies.service';
import {CommonService} from '../services/common.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userForm : FormGroup; //our model driven form variable
  public data : string ;
  public flag = 0;
  public cookiedata; 
  public error_msg;
  public togle : boolean = false;
  constructor(private apiService: ApiService,
  	private _fb: FormBuilder,
    private _cookieservice : CookiesService,
    private _router : Router,
    private commonService: CommonService
  	) { }

  ngOnInit() {
    if(Cookie.get('id') && Cookie.get('token')) {
      this._router.navigateByUrl('/dashboard/(mainpage:mainpage)');
    }
   
    // form fields and validations
  	this.userForm = this._fb.group({
    'email' : [null,Validators.compose([Validators.required,CustomValidators.email])],
    'password' : [null,Validators.required] 
    })

  }

    // submitting user login dataa to the API for authentication
  	submitUser() {
      this.flag = 1;
      // this.commonService.showSnackBar("Logging In..Please Wait!!");
 
  		this.apiService.authenticateUser(this.userForm.value.email, this.userForm.value.password)
  		.subscribe(
  			data => {
                this.flag = 0;
                this.cookiedata = {
                    "email" : data.email,
                    "token": data.token,
                    "id" : data.id,
                    "is_admin": data.is_admin}; 
                this._cookieservice.setCookieData(this.cookiedata); //setting data in the cookie
                console.log(data.is_admin);
                if(data.is_admin == true)
                {
                  console.log('true');
                this._router.navigateByUrl("/dashboard/(mainpage:mainpage)"); //navigating to the dashboard 
                }
                if(data.is_admin == false)
                {
                  console.log('false');
                this._router.navigateByUrl("/dashboard/(usermainpage:usermainpage)"); //navigating to the dashboard 

                }   
                },
        error => {
                  this.flag = 0;  
                   if(error.status != 0) {
                    this.error_msg = JSON.parse(error._body);               
                    this.error_msg = this.error_msg.msg; 
                    this.hide();   
                   }
                   else{
                    this.commonService.showSnackBar("Something went wrong..Please Try Again!");
                   }
                },
  			() => {
                console.log("getting cookie",Cookie.getAll());
              }
  			);
  	}

    hide(){
        this.togle = true;
    }

    
}
