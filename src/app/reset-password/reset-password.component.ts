import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { CommonService } from '../services/common.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  resetForm: FormGroup;
  public token;
  public flag = 0;
  constructor(private _fb: FormBuilder, private _router: Router, private commonService: CommonService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    if (Cookie.get('id') && Cookie.get('token')) {
      this._router.navigateByUrl('/dashboard/(mainpage:mainpage)');
    
    }
  console.log(this.flag);
    this.token = this.activatedRoute.snapshot.params['token'];
    console.log(this.token);

    this.resetForm = this._fb.group({
      passwords: this._fb.group({
        password: [null, Validators.compose([Validators.required, Validators.minLength(6)])],
        confirmPassword: [null, Validators.required]
      }),

    })



    this.subscribeToFormChangesAndSetValidity();
  }



  // matching password and confirmPassword fields
  subscribeToFormChangesAndSetValidity() {
    const myFormValueChanges$ = this.resetForm.controls["passwords"].valueChanges;

    myFormValueChanges$.subscribe(x => {
      if (x.password === x.confirmPassword) {
        this.resetForm.controls["passwords"].setErrors(null); //return null if matches
      } else {
        this.resetForm.controls["passwords"].setErrors({ "notValid": true }); //return error if doesnt match
      }
    });
  }

  resetPassword() {
    this.flag = 1;
    this.commonService.updatePassword(this.resetForm.value.passwords.password, this.token)
      .subscribe(
      data => {
        this.flag = 0;
        console.log(this.flag)
        console.log(data);
        this.commonService.showSnackBar("Password Reset Successfully");
        this._router.navigateByUrl('/login');
      },
      error => {
        console.log(error);
        let body = JSON.parse(error._body);
        this.commonService.showSnackBar(body.msg);
      },
      () => console.log("function called after reset password"))
  }
}
