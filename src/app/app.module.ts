import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterializeModule } from 'angular2-materialize';
import { AppComponent } from './app.component';
import { AppRoutingModule, routingComponents } from './app.routing.module';
import { ApiService } from './services/api.service';
import { CookiesService } from './services/cookies.service';
import { MaterialModule } from '@angular/material';
// import {SignupModule} from './signup/signup.module';
import { AuthGuardService } from './guard/auth-guard.service';
import { AgentsService } from './services/agents.service';
import { TasksService } from './services/tasks.service';
import { PlanService } from './services/plan.service';
import { RulesService } from './services/rules.service';
import { LoadingAnimateModule, LoadingAnimateService } from 'ng2-loading-animate';
import {NgSpinningPreloader} from 'ng2-spinning-preloader';
import {MainpageService} from './services/mainpage.service';
import {CommonService} from './services/common.service';
import {  MdSnackBar } from '@angular/material';
import {PayoutService} from './services/payout.service';
import { SearchPipe } from './search.pipe';
import {GpsRangeService} from './services/gps-range.service';
import {DutyHourService} from './services/duty-hour.service';
import {UserMainpageService} from './services/user-mainpage.service';
import {PayoutReportingService} from './services/payout-reporting.service';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
// import { Ng2CompleterModule } from "ng2-completer";
import { FlashMessagesModule } from 'angular2-flash-messages';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    SearchPipe,
    ResetPasswordComponent
  ],
  imports: [
    BrowserModule,
    // Ng2CompleterModule,
    FormsModule,
    FlashMessagesModule,
    HttpModule,
    MaterializeModule,
    MaterialModule.forRoot(),
    AppRoutingModule,
    ReactiveFormsModule,
     LoadingAnimateModule.forRoot()
    // SignupModule

  ],
  providers: [PayoutReportingService,UserMainpageService,DutyHourService,GpsRangeService,PayoutService,CommonService,MdSnackBar,MainpageService, NgSpinningPreloader ,LoadingAnimateService,ApiService,RulesService,CookiesService,AuthGuardService,AgentsService,TasksService,PlanService],
  bootstrap: [AppComponent]
})
export class AppModule { }
