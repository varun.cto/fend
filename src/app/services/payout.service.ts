import { Injectable } from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Headers} from '@angular/http';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import {CommonService} from './common.service';

@Injectable()
export class PayoutService {
  
  public user_id ;
  public token ;
  constructor(private _http : Http , private commonService : CommonService) {

   this.user_id = Cookie.get('id');
   this.token = Cookie.get('token');
   }


  // get payout of particular agent for particular task with detail 

  getPayout(task_id){
  	  console.log("payout fetched successfuly");
    	let p_url = this.commonService.getApiUrl("getPayoutOfAgent") ;
    	var headers = new Headers();
      var payout = {
            "task_id":task_id
        };
  		// headers.append('User-Id',id);
      // 	headers.append('Token',token);
    	return this._http.post(p_url,payout,{ headers : headers}) 
    	.map(res => res.json())
  }


}
