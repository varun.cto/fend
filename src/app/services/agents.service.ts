import { Injectable } from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Headers} from '@angular/http';
import { LoadingAnimateService } from 'ng2-loading-animate';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import {CommonService} from  './common.service';
@Injectable()
export class AgentsService {

  private user_id = Cookie.get('id');
  private token = Cookie.get('token');

  constructor (private _http : Http,private _loadingSvc: LoadingAnimateService,
    private commonService : CommonService){}

   getAgents(){
    this._loadingSvc.setValue(true);
    let api_url=this.commonService.getApiUrl("tookanAgentsData");
    var headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    return this._http.get(api_url,{ headers : headers}) 
    .map(res => res.json());
  }



  getAgentsFromApi(){
    var headers = new Headers();
    let api_url=this.commonService.getApiUrl("getAllAgents");
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    return this._http.get(api_url,{ headers : headers}) 
    .map(res => res.json());
  }

  updatePlan(fleet_id, plan_a_no, plan_b_no){
    let p_url = this.commonService.getApiUrl("updatePlans");
    var headers = new Headers();
    var plans = {
            "fleet_id":fleet_id,
            "plan_a_no":plan_a_no,
            "plan_b_no":plan_b_no
        };
    headers.append('Content-Type','application/json');
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    return this._http.post(p_url,plans,{ headers : headers}) 
    .map(res => res.json());
  }

  getAgentById(fleet_id){
    let p_url = this.commonService.getApiUrl("getAgentById");
    var headers = new Headers();
    var fleetid = {
            "fleet_id":fleet_id
        };
    headers.append('Content-Type','application/json');
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    return this._http.post(p_url,fleetid,{ headers : headers}) 
    .map(res => res.json());
  }


}
