import { Injectable } from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Headers} from '@angular/http';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import {CommonService} from './common.service';

@Injectable()
export class UserMainpageService {

  public user_id ;
  public token ;
  constructor(private _http : Http, private commonService : CommonService) { 
   this.user_id = Cookie.get('id');
   this.token = Cookie.get('token');
 }


// get all completed tasks count from own api
  userMainpage(id,token,request_url){
          let api_url=this.commonService.getApiUrl(request_url);
    var headers = new Headers();
    var dt = {
            "id":id
        };
    headers.append('Content-Type','application/json');
    headers.append('User-Id',id);
    headers.append('Token',token);
    return this._http.post(api_url,dt,{ headers : headers}) 
    .map(res => res.json());
  }



  // // get all completed tasks count from own api
  // getTaskCompleteCount(id,token){
  //   	    let api_url=this.commonService.getApiUrl("successfulTaskCountOfAgent");
  //   var headers = new Headers();
  //   var dt = {
  //           "id":id
  //       };
  //   headers.append('Content-Type','application/json');
  //   headers.append('User-Id',id);
  //   headers.append('Token',token);
  //   return this._http.post(api_url,dt,{ headers : headers}) 
  //   .map(res => res.json());
  // }

  // // get all canceled tasks count from own api
 
  // getTaskCancelCount(id,token){
    	
  //         let api_url=this.commonService.getApiUrl("cancelTaskCountOfAgent");
  //   var headers = new Headers();
  //   var dt = {
  //           "id":id
  //       };
  //   headers.append('Content-Type','application/json');
  //   headers.append('User-Id',id);
  //   headers.append('Token',token);
  //   return this._http.post(api_url,dt,{ headers : headers}) 
  //   .map(res => res.json());
  // }

  // // get all Total tasks count from own api
 
  // getTotalTaskCount(id,token){
  //   let api_url=this.commonService.getApiUrl("taskCountOfAgent");
  //   var headers = new Headers();
  //   var dt = {
  //           "id":id
  //       };
  //   headers.append('Content-Type','application/json');
  //   headers.append('User-Id',id);
  //   headers.append('Token',token);
  //   return this._http.post(api_url,dt,{ headers : headers}) 
  //   .map(res => res.json());
  // }


  // // get Recent tasks Of Particular Agent from own api
  // getRecentTasks(id,token){
    	
  //   let api_url=this.commonService.getApiUrl("recentTaskOfAgent");
  //   var headers = new Headers();
  //   var dt = {
  //           "id":id
  //       };
  //   headers.append('Content-Type','application/json');
  //   headers.append('User-Id',id);
  //   headers.append('Token',token);
  //   return this._http.post(api_url,dt,{ headers : headers}) 
  //   .map(res => res.json());
  // }

  // // get new payouts from own api
  // getTotalPayouts(id,token){
    	
  //    let api_url=this.commonService.getApiUrl("totalPayoutOfAgent");
  //   var headers = new Headers();
  //   var dt = {
  //           "id":id
  //       };
  //   headers.append('Content-Type','application/json');
  //   headers.append('User-Id',id);
  //   headers.append('Token',token);
  //   return this._http.post(api_url,dt,{ headers : headers}) 
  //   .map(res => res.json());
  // }

}
