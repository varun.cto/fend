import { Injectable } from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Headers} from '@angular/http';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import {CommonService} from './common.service';
import { LoadingAnimateService } from 'ng2-loading-animate';

@Injectable()

export class MainpageService {
  
  public user_id ;
  public token ;
  constructor(private _http : Http, private commonService : CommonService,
							private _loadingSvc: LoadingAnimateService) { 
   this.user_id = Cookie.get('id');
   this.token = Cookie.get('token');
 }

  // get all tasks count from own api
  getTaskCount(id,token){
			this._loadingSvc.setValue(true);
    	let p_url = this.commonService.getApiUrl("taskCount") ;
    	var headers = new Headers();
  		headers.append('User-Id',id);
    	headers.append('Token',token);
    	return this._http.get(p_url,{ headers : headers}) 
    	.map(res => res.json())
  }

  // get all plans count from own api
  getPlanCount(id,token){
			this._loadingSvc.setValue(true);    	
    	let p_url = this.commonService.getApiUrl("planCount");
    	var headers = new Headers();
  		headers.append('User-Id',id);
      headers.append('Token',token);
    	return this._http.get(p_url,{ headers : headers}) 
    	.map(res => res.json())
  }

  // get all agents count from own api
  getAgentCount(id,token){
			this._loadingSvc.setValue(true);    	
    	let p_url = this.commonService.getApiUrl("agentCount") ;
    	var headers = new Headers();
  		headers.append('User-Id',id);
      headers.append('Token',token);
    	return this._http.get(p_url,{ headers : headers}) 
    	.map(res => res.json())
  }


  // get new tasks from own api
  getNewTasks(id,token){
			this._loadingSvc.setValue(true);    	
    	let p_url = this.commonService.getApiUrl("newTasks") ;
    	var headers = new Headers();
  		headers.append('User-Id',id);
      headers.append('Token',token);
    	return this._http.get(p_url,{ headers : headers}) 
    	.map(res => res.json())
  }

  // get new agents from own api
  getNewAgents(id,token){
			this._loadingSvc.setValue(true);    	
    	let p_url = this.commonService.getApiUrl("newAgents") ;
    	var headers = new Headers();
  		headers.append('User-Id',id);
      headers.append('Token',token);
    	return this._http.get(p_url,{ headers : headers}) 
    	.map(res => res.json())
  }
}
