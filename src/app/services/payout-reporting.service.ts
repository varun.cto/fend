import { Injectable } from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Headers} from '@angular/http';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import {CommonService} from './common.service';
import { LoadingAnimateService } from 'ng2-loading-animate';

@Injectable()
export class PayoutReportingService {

  public user_id ;
  public token ;
  constructor(private _http : Http , private commonService : CommonService,
              private _loadingSvc: LoadingAnimateService) {

   this.user_id = Cookie.get('id');
   this.token = Cookie.get('token');
   }

 // Fetch All Pay Out From API

fetchPayoutFromApi(){
  this._loadingSvc.setValue(true);
    var headers = new Headers();
    let api_url=this.commonService.getApiUrl("agents");
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    return this._http.get(api_url,{ headers : headers}) 
    .map(res => res.json());
  }

  /*Fetch Pay Out  for Particular search Criteria From API*/

  getPayoutById(id){
    var headers = new Headers();
    let api_url=this.commonService.getApiUrl("plans/") + id;
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    return this._http.get(api_url,{ headers : headers}) 
    .map(res => res.json());
  }

  // get payout Reporting of particular agent for particular task with detail 

  getPayoutReporting(task_id){
  	  console.log("payout fetched successfuly");
    	let p_url = this.commonService.getApiUrl("getPayoutOfAgent") ;
    	var headers = new Headers();
      var payout = {
            "task_id":task_id
        };
  		// headers.append('User-Id',id);
      // 	headers.append('Token',token);
    	return this._http.post(p_url,payout,{ headers : headers}) 
    	.map(res => res.json())
  }

  // Search Task Agent Wise From Api
   SearchResulFromApi(fleet_id,from,to){
   this._loadingSvc.setValue(true);     
    let api_url = this.commonService.getApiUrl("fetchAgentTaskOfTime");
    var headers = new Headers();
    var search = {
            "fleet_id" : fleet_id,
            "date1" : from,
            "date2": to
        };
    headers.append('Content-Type','application/json');
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    return this._http.post(api_url,search,{ headers : headers}) 
    .map(res => res.json());
  }

}
