import { Injectable } from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Headers} from '@angular/http';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import {CommonService} from './common.service';
@Injectable()
export class PlanService {
  
  public plan_data;
  public user_id ;
  public token ;

  constructor(private _http : Http, private commonService : CommonService)  {
       this.user_id = Cookie.get('id');
       this.token = Cookie.get('token');
     }

  // saving plans to the database
  addPlanToApi(type,name){
    let api_url = this.commonService.getApiUrl("plans");
    var headers = new Headers();
    var plan = {
            "plan_type" : type,
            "name" : name
        };
    headers.append('Content-Type','application/json');
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    return this._http.post(api_url,plan,{ headers : headers}) 
    .map(res => res.json());
  }

  fetchPlansFromApi(){
    var headers = new Headers();
    let api_url=this.commonService.getApiUrl("plans");
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    return this._http.get(api_url,{ headers : headers}) 
    .map(res => res.json());
  }

  getPlanById(id){
    var headers = new Headers();
    let api_url=this.commonService.getApiUrl("plans/") + id;
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    return this._http.get(api_url,{ headers : headers}) 
    .map(res => res.json());
  }

  updatePlan(id,plan_type,name){
    console.log(id,plan_type,name)
    let api_url=this.commonService.getApiUrl("updateplan");
    var headers = new Headers();
    var plan = {
            "id" : id,
            "plan_type" : plan_type,
            "name" : name
        };
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    headers.append('Content-Type','application/json');
    return this._http.post(api_url,plan,{ headers : headers}) 
    .map(res => res.json());
  }


  makeDefault(id){
    console.log(id)
    let api_url=this.commonService.getApiUrl("updateDefaultPlan");
    var headers = new Headers();
    var defaultPlan = {
            "id" : id
        };
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    headers.append('Content-Type','application/json');
    return this._http.post(api_url,defaultPlan,{ headers : headers}) 
    .map(res => res.json());
  }

  fetchDefaultPlansFromApi(){
    var headers = new Headers();
    let api_url=this.commonService.getApiUrl("plans");
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    return this._http.get(api_url,{ headers : headers}) 
    .map(res => res.json());
  }


}
