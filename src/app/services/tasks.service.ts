import { Injectable } from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Headers} from '@angular/http';
import { LoadingAnimateService } from 'ng2-loading-animate';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import {CommonService} from './common.service';
@Injectable()
export class TasksService {

  private apiKey = "5630c117f731fb77b18381bb485929ab540f37e1fa94b5d8d52dbab88ecbd343";
  private _url = "https://api.tookanapp.com/v2/";
  private _apiurl = "http://localhost:3000/v1/";
  public user_id ;
  public token ;


  constructor (private _http : Http,
    private _loadingSvc: LoadingAnimateService , private commonService : CommonService){

   this.user_id = Cookie.get('id');
   this.token = Cookie.get('token');
  }


   syncTasksFromTookan(){
    this._loadingSvc.setValue(true);
    let p_url = this.commonService.getApiUrl("tookanTasks");
    var headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('User-Id',this.user_id);
     headers.append('Token',this.token);
    return this._http.get(p_url,{ headers : headers}) 
    .map(res => res.json())
  }

   fetchTaskDetailFromApi(address,start_time,finish_time,name){
    let api_url=this.commonService.getApiUrl("taskDetail");
    var headers = new Headers();
    var dt = {
            "job_address":address,
            "job_pickup_datetime": start_time,
            "job_delivery_datetime":finish_time,
            "customer_username":name
        };
    headers.append('Content-Type','application/json');
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    return this._http.post(api_url,dt,{ headers : headers}) 
    .map(res => res.json());
  }

  addAssignTaskToApi(job_id,fleet_id,plan_id){
    let api_url=this.commonService.getApiUrl("task_assigned");
    var headers = new Headers();
    var assigned_task = {
            "task_id":job_id,
            "fleet_id": fleet_id,
            "plan_id":plan_id
        };
    headers.append('Content-Type','application/json');
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    return this._http.post(api_url,assigned_task,{ headers : headers}) 
    .map(res => res.json());
  }

  fetchJobId(job_id){
    let api_url=this.commonService.getApiUrl("taskExist");
    var headers = new Headers();
    var task = {
            "task_id":job_id,
        };
    headers.append('Content-Type','application/json');
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    return this._http.post(api_url,task,{ headers : headers}) 
    .map(res => res.json());
  }

  updatePlanAssignedToTask(task_id,plan_id){
    let api_url=this.commonService.getApiUrl("updateTaskAssigned");
    var headers = new Headers();
    var update_plan = {
            "task_id":task_id,
            "plan_id":plan_id
        };
    headers.append('Content-Type','application/json');
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    return this._http.post(api_url,update_plan,{ headers : headers}) 
    .map(res => res.json());
  }

  fetchTaskDetailOfAgent(user_id){
    let api_url=this.commonService.getApiUrl("fetch_task_assigned_to_agent")  ;
    var headers = new Headers();
    var tasks = {
            "user_id":user_id
        };
    headers.append('Content-Type','application/json');
    // headers.append('User-Id',this.user_id);
    // headers.append('Token',this.token);
    return this._http.post(api_url,tasks,{ headers : headers}) 
    .map(res => res.json())
  }
}
