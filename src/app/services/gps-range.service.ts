import { Injectable } from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Headers} from '@angular/http';
import { LoadingAnimateService } from 'ng2-loading-animate';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import {CommonService} from  './common.service';
@Injectable()
export class GpsRangeService {

  constructor(private _http : Http,private _loadingSvc: LoadingAnimateService,
    private commonService : CommonService) { }

  fetchGpsRange(){
    let api_url=this.commonService.getApiUrl("gpsranges");
    return this._http.get(api_url) 
    .map(res => res.json());
  }

  submitGpsRange(id,range){
    let api_url=this.commonService.getApiUrl("updateGpsRange");
    var headers = new Headers();
    var dt = {
            "id":id,
            "range":range
        };
    headers.append('Content-Type','application/json');
    return this._http.post(api_url,dt,{ headers : headers}) 
    .map(res => res.json());


  	}	
}
