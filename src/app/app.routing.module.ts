import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import {LoginComponent} from './login/login.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AuthGuardService} from './guard/auth-guard.service';
import {MainpageComponent} from './dashboard/mainpage/mainpage.component';
import { TasksComponent } from './dashboard/tasks/tasks.component';
import { AgentsComponent } from './dashboard/agents/agents.component';
import {PlansComponent} from './dashboard/plan-menu/plans/plans.component';
import {ShowPlansComponent} from './dashboard/plan-menu/show-plans/show-plans.component';
import {PlanMenuComponent} from './dashboard/plan-menu/plan-menu.component';
import {ForgotPasswordComponent}  from './forgot-password/forgot-password.component';
import {UpdatetaskPlanComponent}  from './dashboard/tasks/updatetask-plan/updatetask-plan.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {PayoutComponent} from './dashboard/payout/payout.component';
import {ShowTasksComponent} from './dashboard/payout/show-tasks/show-tasks.component';
import {GpsRangeComponent} from './dashboard/gps-range/gps-range.component';
import {DutyHourComponent} from './dashboard/duty-hour/duty-hour.component';
import {PayoutSearchComponent} from './dashboard/payout-report/payout-search/payout-search.component';
import {PayoutSearchResultComponent} from './dashboard/payout-report/payout-search-result/payout-search-result.component';
import {PayoutReportComponent} from './dashboard/payout-report/payout-report/payout-report.component';
import {UserMainpageComponent} from './dashboard/user-mainpage/user-mainpage.component';
import {DefaultPlanComponent} from  './dashboard/plan-menu/default-plan/default-plan.component';
import{ResetPasswordComponent} from './reset-password/reset-password.component';
import { DummyTaskComponent } from './dashboard/tasks/dummy-task/dummy-task.component';

const appRoutes: Routes = [
	{path: '', pathMatch:'full', redirectTo:'signup'},
	{path: 'signup', component: SignupComponent},
	{path: 'login', component: LoginComponent},
	{path: 'forgot', component: ForgotPasswordComponent},
	{path: 'resetpassword/:token', component: ResetPasswordComponent},

	{path: 'dashboard', 
		component: DashboardComponent,
		canActivateChild: [AuthGuardService],children:[
		{path: 'agent',component : AgentsComponent,outlet:'agent'},
		{path: 'mainpage', component : MainpageComponent,outlet:'mainpage'},
		{path: 'task', component : TasksComponent,outlet:'task'},
		{path: 'plans', component : PlansComponent,outlet:'plans'},
		{path: 'showplans', component : ShowPlansComponent,outlet:'showplans'},
		{path: 'planmenu', component : PlanMenuComponent, outlet:'planmenu'},
 		{path: 'updatetaskplan/:taskid/:fleetid', component : UpdatetaskPlanComponent, outlet:'updatetaskplan'},
		{path: 'payout/:id', component : PayoutComponent, outlet:'payout'},
		{path: 'showtasks', component : ShowTasksComponent, outlet:'showtasks'},
		{path: 'gpsrange', component : GpsRangeComponent, outlet:'gpsrange'},
		{path: 'dutyhour', component : DutyHourComponent, outlet:'dutyhour'},
		{path: 'payoutsearch', component : PayoutSearchComponent, outlet:'payoutsearch'},
		{path: 'payoutsearchresult', component : PayoutSearchResultComponent, outlet:'payoutsearchresult'},
		{path: 'payoutreport/:id', component : PayoutReportComponent, outlet:'payoutreport'},
		{path: 'usermainpage', component : UserMainpageComponent,outlet:'usermainpage'},
		{path: 'defaultplan', component : DefaultPlanComponent, outlet:'defaultplan'},
		{path: 'dummy', component : DummyTaskComponent, outlet:'dummy'},

		{path:'**',redirectTo:'404'}
		
		]
	},
	{path:'404',component:NotFoundComponent},
	{path:'**',redirectTo:'404'}

];

@NgModule({
	imports: [RouterModule.forRoot(appRoutes)],
	exports: [RouterModule]
})

export class AppRoutingModule{
}

export const routingComponents = [DummyTaskComponent,ResetPasswordComponent,DefaultPlanComponent,UserMainpageComponent, PayoutReportComponent,PayoutSearchResultComponent,PayoutSearchComponent,DutyHourComponent,GpsRangeComponent,PayoutComponent,ShowTasksComponent,NotFoundComponent,UpdatetaskPlanComponent,ForgotPasswordComponent,PlanMenuComponent,ShowPlansComponent,PlansComponent,MainpageComponent,SignupComponent,LoginComponent,DashboardComponent, TasksComponent, AgentsComponent];