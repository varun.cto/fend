import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import {FormBuilder,FormGroup,Validators} from '@angular/forms';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import {CookiesService} from '../services/cookies.service';
import { Router  } from '@angular/router';
import {  CommonService } from '../services/common.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  userForm: FormGroup; // our model driven form
  public data : string;
  public error_msg; 
  public status : boolean;
  public status_msg;
  public cookiedata; // saves cookie data
  public check :boolean = false;
  public cross : boolean = false;

  constructor(private apiService: ApiService,
  	private _fb:FormBuilder,
    private _cookieservice : CookiesService,
    private _router : Router,
   public showMessage: CommonService
  	) { }

  ngOnInit() {
    if(Cookie.get('id') && Cookie.get('token')) {
      this._router.navigateByUrl('/dashboard/(mainpage:mainpage)');
    }
    // form fields and validations
  	this.userForm = this._fb.group({
  		'email' : [null,Validators.compose([Validators.required])],
      passwords: this._fb.group({
        password: ['', Validators.compose([Validators.required,Validators.minLength(6)])],
        confirmPassword: ['', Validators.required]
        }),
  	})

    this.subscribeToFormChangesAndSetValidity(); 
  } 

  // matching password and confirmPassword fields
  subscribeToFormChangesAndSetValidity() {
      const myFormValueChanges$ = this.userForm.controls["passwords"].valueChanges;

      myFormValueChanges$.subscribe(x => {
          if(x.password === x.confirmPassword) {
              this.userForm.controls["passwords"].setErrors(null); //return null if matches
          } else {
              this.userForm.controls["passwords"].setErrors({ "notValid" : true}); //return error if doesnt match
          }
      });
  }

  // checking whether email is unique or not
  checkEmail(value){
    this.apiService.checkEmail(value)
        .subscribe(
          data=>{
                this.show(data.msg);
                this.error_msg = data.msg;
                this.status = false ;
                },
          error =>{
            if(error.status != 0) {
             this.error_msg = JSON.parse(error._body); //fetching error message
             this.error_msg = this.error_msg.msg;
             this.show(this.error_msg);
             this.status = true; 
            }
                 },
          ()=> console.log("email validation done"));
  }

  // submitting user signup data to API 
  submitUser() {
    // this.showMessage.showMessage("Signing Up...Please Wait" , "success");
    this.apiService.postUserData(this.userForm.value.email,this.userForm.value.passwords.password)
         .subscribe(
           data => {
                     this.cookiedata = {
                      "email" : data.email,
                      "token": data.token,
                      "id" : data.id,
                      "is_admin": data.is_admin};
                     this._cookieservice.setCookieData(this.cookiedata); //saving data in the cookie
                   },

           error => {
             if(error.status != 0) {
               this.error_msg = JSON.parse(error._body);
               this.status_msg = this.error_msg.msg;
             }
             else{
              this.showMessage.showSnackBar("Something went wrong..Please Try again");
             }
          },

           ()=> {
                 console.log(Cookie.getAll());
                 if(Cookie.get('token') && Cookie.get('id') ) {

                   if(Cookie.get('is_admin') == 'true' )
                   {
                   this._router.navigateByUrl("/dashboard/(mainpage:mainpage)"); //navigating to the dashboard  
                   }
                   if(Cookie.get('is_admin') == 'false' )
                   {
                   this._router.navigateByUrl("/dashboard/(usermainpage:usermainpage)"); //navigating to the dashboard  
                   }
               }
                 else{
                   this._router.navigateByUrl("/login"); //navigating to the login  

                 }
                 });
  }

 show(data){
        
      if(data === "Email already exist")
      {
        console.log("hello");
        this.cross = true;
        this.check = false;
      }
      else if (data == "Email Valid"){
            this.check = true;
            this.cross = false;
      }
  }
  
}
