import { Component, OnInit } from '@angular/core';
import {MainpageService} from '../../services/mainpage.service';
import { Router  } from '@angular/router';
import {CommonService} from '../../services/common.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import {UserMainpageService} from '../../services/user-mainpage.service';
@Component({
  selector: 'app-user-mainpage',
  templateUrl: './user-mainpage.component.html',
  styleUrls: ['./user-mainpage.component.css']
})
export class UserMainpageComponent implements OnInit {

  public task_complete;
	public task_cancel;
	public task_total;
	public recent_tasks;
	public total_payouts;d
  constructor(private userMainPageService : UserMainpageService,
  	private router:Router,
  	private commonService:CommonService ) { }

  ngOnInit() {
	
	this.userMainPageService.userMainpage(Cookie.get('id'),Cookie.get('token'),"successfulTaskCountOfAgent")
		.subscribe(
				data=> {this.task_complete = data },
				error=>console.log(error),
				()=>console.log("task completed count fetched")
			);  	


	this.userMainPageService.userMainpage(Cookie.get('id'),Cookie.get('token'),"cancelTaskCountOfAgent")
		.subscribe(
				data=> {this.task_cancel = data },
				error=>console.log(error),
				()=>console.log("task Canceled fetched")
			);  	

	
	this.userMainPageService.userMainpage(Cookie.get('id'),Cookie.get('token'),"taskCountOfAgent")
		.subscribe(
				data=> {this.task_total = data },
				error=>console.log(error),
				()=>console.log("Total Tasks Done By Agent count fetched")
				  );  


	this.userMainPageService.userMainpage(Cookie.get('id'),Cookie.get('token'), "recentTaskOfAgent" )
		.subscribe(
				data=> {this.recent_tasks = data },
				error=>console.log(error),
				()=>console.log("Recent tasks of Agent fetched")
				  );
	this.userMainPageService.userMainpage(Cookie.get('id'),Cookie.get('token'),"totalPayoutOfAgent")
		.subscribe(
					data=> {this.total_payouts = data;
					console.log(data) },
					error=>console.log(error),
					()=>console.log("New payout fetched")
				  );
  }

}
