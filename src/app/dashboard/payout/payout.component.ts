import { Component, OnInit } from '@angular/core';
import {PayoutService} from '../../services/payout.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-payout',
  templateUrl: './payout.component.html',
  styleUrls: ['./payout.component.css']
})
export class PayoutComponent implements OnInit {

	public job_id:any;
	public payout:any;
  constructor( private payoutService : PayoutService, private route: ActivatedRoute) { }

  ngOnInit() {
  	this.job_id = this.route.snapshot.params['id'];
  	this.getPayout();
  }

  getPayout(){
  	this.payoutService.getPayout(this.job_id)
  		.subscribe(data=>{console.log(data);
  					this.payout = data;},
  			    error=>console.log(error),
  			    ()=>console.log("function called after calculating payout for agent"));
  }

}
