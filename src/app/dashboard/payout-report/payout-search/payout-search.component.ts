import { Component, OnInit } from '@angular/core';
import {PayoutReportingService} from '../../../services/payout-reporting.service';
import { LoadingAnimateService } from 'ng2-loading-animate';
// import { CompleterService, CompleterData } from 'ng2-completer';
import {FormBuilder,FormGroup,Validators} from '@angular/forms';
import { Router  } from '@angular/router';
import {  CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-payout-search',
  templateUrl: './payout-search.component.html',
  styleUrls: ['./payout-search.component.css']
})
export class PayoutSearchComponent implements OnInit {
	public payout;
  payoutForm: FormGroup;
  private searchStr: string;

  private searchData  ;
  public tasks;

  constructor( private payoutReporting:PayoutReportingService,
   			   private _loadingSvc: LoadingAnimateService,           
           private _fb:FormBuilder,
           private _router : Router,
          public commonService: CommonService
) { 
    
  }

  ngOnInit() {	
  	this.payoutReporting.fetchPayoutFromApi()
    .subscribe(
      data=> { this.searchData = data;
              console.log(this.searchData);
              this._loadingSvc.setValue(false);
                       console.log("Agents for payouts fetched");
                      },
      error=>console.log(error),
      ()=>console.log("payouts fetched Called")); 

    this.payoutForm = this._fb.group({
      'fleet_id' : [null,Validators.compose([Validators.required])],
      'from' : [null,Validators.compose([Validators.required])],
      'to' : [null,Validators.compose([Validators.required])],

    }); 
    
  	 }

  // fetchPayout(){
  //   // fetching all payout
  //   this.payoutReporting.fetchPayoutFromApi()
  //   .subscribe(data=> { this.searchData = JSON.stringify(data);
                        
  //                        this._loadingSvc.setValue(false);
  //                      console.log("Agents for payouts fetched");
  //                     },
  //     error=>console.log(error),
  //     ()=>console.log("payouts fetched Called")); 
  // }

    checkPayout(id){
     // navigating to different pages
    this._router.navigateByUrl("/dashboard/(payoutreport:payoutreport/"+id+")") ;
   }

// adding plans
  submitPayout(){
    this.commonService.showMessage("Searching in Process...","success");
    this.payoutReporting.SearchResulFromApi(this.payoutForm.value.fleet_id,this.payoutForm.value.from,this.payoutForm.value.to)
      .subscribe(
        data => {
          this.tasks = data;
            this._loadingSvc.setValue(false); 
          console.log(data);
          // this._router.navigateByUrl('/dashboard/(showplans:showplans)')
          },
        error=> console.log(error),
        ()=>console.log("Agent data fetched"));
  }
 

}
