/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { GpsRangeComponent } from './gps-range.component';

describe('GpsRangeComponent', () => {
  let component: GpsRangeComponent;
  let fixture: ComponentFixture<GpsRangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpsRangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpsRangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
