 import { Component, OnInit,EventEmitter } from '@angular/core';
import {AgentsService} from '../../services/agents.service';
import {FormBuilder,FormGroup,NgForm,Validators} from '@angular/forms';
import {PlanService} from '../../services/plan.service';
import { LoadingAnimateService } from 'ng2-loading-animate';
import {  CommonService } from '../../services/common.service';

@Component({
  selector: 'app-agents',
  templateUrl: './agents.component.html',
  styleUrls: ['./agents.component.css']
})

export class AgentsComponent implements OnInit {

 private agents;
 public agent_id;
 public plans;

 constructor(private agentsService: AgentsService,
   private _fb:FormBuilder,
   private planService: PlanService,
   private _loadingSvc: LoadingAnimateService,
   public showSnackbar: CommonService
   ){ }

  getId(id){
    this.agent_id = id;
  }

  ngOnInit() {
    this.showSnackbar.showMessage("Fetching Agents Detail" , "success");
     this.agentsService.getAgents()
       .subscribe(
         data => {this.agents = data;
         this._loadingSvc.setValue(false);},
         error => {console.log(error);
           this.showSnackbar.showMessage(" Error While Fetching Agents Detail" , "danger");},
 
         ()=> console.log("function called after fetching agents from api"));
  }

  selectPlan(){
    this.planService.fetchPlansFromApi()
      .subscribe(
        data=>{this.plans=data;
               this._loadingSvc.setValue(false);
              },
        error=>console.log(error),
        ()=>console.log("plans fetched"));
  }
  
  // updating plans of agent
  updatePlan(form : NgForm,agent_name){

    if(form.value.plan_a_no == form.value.plan_b_no){
        this.showSnackbar.showSnackBar("Plan A and B can not be same.");        
    }

    else{

          this.agentsService.updatePlan(this.agent_id,form.value.plan_a_no,form.value.plan_b_no)
          .subscribe(
            data => {
                  console.log(data);
                  this.showSnackbar.showSnackBar("Plans Assigned to " + agent_name + " successfully");
                  this.agentsService.getAgentsFromApi()
                    .subscribe(
                      data=>{
                        this.agents=data;},
                      error=>console.log(error),
                      ()=>console.log("function called after fetching agents.")
                      );
                  },
        error => {
          console.log(error);
            this.showSnackbar.showSnackBar("Error while assigning plans.");
          },
        () => {
            console.log("Function called after agent update plan call");
          }
        );
    }
  }

}
