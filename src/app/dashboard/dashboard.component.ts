import { Component, OnInit } from '@angular/core';
import { Router  } from '@angular/router';
import {NgZone} from '@angular/core';
import {ApiService} from '../services/api.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { NgModule } from '@angular/core';
import { CommonService } from '../services/common.service';
import { AgentsService } from '../services/agents.service';
import { LoadingAnimateService } from 'ng2-loading-animate';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public sid : boolean;
  public mod ;
  public width ;
  public height;
  public path;
  public valueagent;
  public user ;
  public is_admin;
  constructor(private ngZone:NgZone,
              private router:Router,
              private apiService:ApiService,
              private commonService:CommonService,
              private agentsService:AgentsService,
              private _loadingSvc: LoadingAnimateService) { 
   
  		//  window.onresize = (e) =>
    // {
        ngZone.run(() => {
            this.width = window.innerWidth;
            this.height = window.innerHeight;
            if(this.width <= 850 )
            {
              this.sid = false;
              this.mod = "over";
            }
            else{
              
              this.sid = true;
              this.mod = "side";

            }
        });
    }
  // }

  ngOnInit() {
    this._loadingSvc.setValue(false);
      this.user = Cookie.get('email')
      this.user =   this.user.substring(0, this.user.lastIndexOf("@"));
      this.is_admin = Cookie.get('is_admin');

     this.agentsService.getAgents()
       .subscribe(
         data => {console.log(data);
                  this._loadingSvc.setValue(false);      
                   },
         error => console.log(error),
         ()=> console.log("function called after fetching agents from api"));
  }
     
deleteCookie(){
 
    location.reload();
    Cookie.deleteAll();
  
}

  // logout from the asasfapplication
  logout(){
    this.apiService.logout(Cookie.get('id'),Cookie.get('token'))
        .subscribe(
          data =>{
            Cookie.deleteAll();
            console.log(Cookie.get('id'),"Succesfully logout");
            if(Cookie.get('id')){
            this.deleteCookie();
            }
            this.router.navigateByUrl('/login') ;
          },
          error =>{
            Cookie.deleteAll();
            if(Cookie.get('id')){
            this.deleteCookie();
            }
            this.router.navigateByUrl('/login') ;                      
          },
          ()=>{
            console.log(Cookie.get('id'),"function called after logging out");
            
          }
        );
  }

}
// asdf