 import { Component, OnInit } from '@angular/core';
import {PlanService} from '../../../services/plan.service';
import {RulesService} from '../../../services/rules.service';
import { Router  } from '@angular/router';
import {FormBuilder,FormGroup,NgForm,Validators} from '@angular/forms';
import { LoadingAnimateService } from 'ng2-loading-animate';
import {CommonService} from '../../../services/common.service';

@Component({
  selector: 'app-show-plans',
  templateUrl: './show-plans.component.html',
  styleUrls: ['./show-plans.component.css']
})
export class ShowPlansComponent implements OnInit {

  public plans;
  planForm: FormGroup;
  public data;
  public rules =[];
  public planDetail = [];

  constructor(
    private planService: PlanService,
    private rulesService: RulesService,
    private router: Router,
    private _fb:FormBuilder,
    private _loadingSvc: LoadingAnimateService,
    private commonService:CommonService) { }

  ngOnInit() {
    // this.planForm = this._fb.group({
    //   'id' : [null,Validators.compose([Validators.required])],
    //   'plan_type' : [null,Validators.compose([Validators.required])],
    //   'name' : [null,Validators.compose([Validators.required])]    
    // })
    this.fetchPlans();
  }


  fetchPlans(){
    // fetching all plans
    this.planService.fetchPlansFromApi()
    .subscribe(data=> { this.plans = data;
                         this._loadingSvc.setValue(false);
                      },
      error=>console.log(error),
      ()=>console.log("plans fetched")); 
  }

  // navigating to the edit page
  editPlan(id){
    this.planService.getPlanById(id)
    .subscribe(data=>
      { 
        // this.data = data;
        this.planDetail[id] = data;
        console.log(this.planDetail);
         // this.planDetail[id] = {
         //      id:this.data.id,
         //      plan_type: this.data.plan_type,
         //      name : this.data.name,
         //      };
         //      console.log(this.planDetail[id])
         // this.planForm.patchValue(this.planDetail[id]);
      },
      error => console.error(error),
      ()=> console.log("finished"));
  }

  makeDefault(plan_id){

    this.planService.makeDefault(plan_id)
    .subscribe(data=>{ console.log("updated Default Plan");
      this.commonService.showSnackBar("Default Plan Updated");
     },
     error => console.log(error),
     ()=> console.log("Function Called after Updating Plan"));
  }

//updating plan
  updatePlan(form :NgForm){
    this.planService.updatePlan(form.value.id,form.value.plan_type,form.value.name)
      .subscribe(
        data => {console.log(data);
                  this.fetchPlans();
                  this.commonService.showSnackBar("Plan Updated Successfuly")
                },
        error=> console.log(error),
        ()=>console.log("plan edited"));
  }

//fetching rules of particular plan
  getRules(id){
    this.rulesService.getRulesByPlan(id)
    .subscribe(
      data => {
        console.log(data);
        this.rules[id] = data;
      },
      error => console.log(error),
      () => console.log("rules fetched")
      );
  }

//updating amount of particular rule
  updateRuleAmount(form : NgForm,rule_type){
    this.rulesService.updateRule(form.value.id, form.value.amount)
    .subscribe(
      data => {console.log(data);
               this.commonService.showSnackBar( rule_type + " Amount Updated" );
              },
      error => console.log(error),
      () => console.log("rules amount updated")
      );

  }
}
