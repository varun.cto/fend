import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,Validators} from '@angular/forms';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Router  } from '@angular/router';
import {PlanService} from '../../../services/plan.service';
import {RulesService} from '../../../services/rules.service';
import {CommonService} from '../../../services/common.service';

@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.css']
})
export class PlansComponent implements OnInit {

  planForm: FormGroup;
  public flag = 0;
  constructor(private _fb:FormBuilder,
    private _router : Router,
    private planService: PlanService,
    private rulesService: RulesService,
    private commonService:CommonService) { }

  ngOnInit() {
  	this.planForm = this._fb.group({
  	  'type' : [null,Validators.compose([Validators.required])],
  	  'name' : [null,Validators.compose([Validators.required])]    
  	})
  }

  // adding plans
  submitPlan(){
    this.flag = 1;
  	this.planService.addPlanToApi(this.planForm.value.type,this.planForm.value.name)
  		.subscribe(
  			data => {
          console.log(data);
          this.commonService.showSnackBar("Plan added successfully");
          this._router.navigateByUrl('/dashboard/(showplans:showplans)');
          
          },
  			error=> {console.log(error);
          this.flag = 0;   
          this.commonService.showSnackBar("Plan name already taken.Please choose different one.");
        },
  			()=>console.log("plans addded"));
  }

}


