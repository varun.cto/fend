/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DutyHourComponent } from './duty-hour.component';

describe('DutyHourComponent', () => {
  let component: DutyHourComponent;
  let fixture: ComponentFixture<DutyHourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DutyHourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DutyHourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
